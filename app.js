var express = require('express')
var path = require('path')
var favicon = require('serve-favicon')
var logger = require('morgan')
var bodyParser = require('body-parser')
var robots = require('express-robots-txt')
// var projectConfig = require('./modules/config-middleware')

/* =============================
    Routes files declarations
============================= */
// index pages
var dashboard = require('./routes/dashboard/index')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.use(favicon(path.join(__dirname, 'assets/images', 'favicon.png')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'assets')))
app.use(express.static(path.join(__dirname, 'vendors')))

/* Set public folder for sitemaps, pdf etc.. */
app.use(express.static(path.join(__dirname, 'public')))
/* Set robots.txt with express middleware, for the sitemap, place it inside /public folder */
if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'staging' || process.env.NODE_ENV === undefined) {
  /* Disallow bots for development/staging */
  app.use(robots({UserAgent: '*', Disallow: '/'}))
}
if (process.env.NODE_ENV === 'production') {
  /* Allow bots for production */
  app.use(robots({UserAgent: '*', Disallow: ['/cookies-policy'], Sitemap: '/sitemap.xml'}))
}

/* =============================
       Routes definitions
============================= */
// index pages routes
app.use('/', dashboard)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404).render('error/404/index')
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.render('error', {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app
