$(function () {
  /* Select the mobile menu elements */
  const mobileMenu = $('.js-mobile-menu').find('nav')
  const submenuList = $('.js-submenu')

  /* Hide Nav and submenus */
  mobileMenu.slideUp()
  submenuList.slideUp()

  /* Toggle mobile menu on click */
  $('.js-burger-menu').click(() => {
    mobileMenu.slideToggle(100, () => {
      // find active primary menu items and open their submenu
      $('a.active').siblings(submenuList).slideDown()
      // find active submenu items and open their submenu
      $('a.active').closest(submenuList).slideDown()
    })
  })

  /* Slide down the submenus */
  $('.js-open-submenu').click(function () {
    $(this).next(submenuList).slideToggle(400)
  })
})
